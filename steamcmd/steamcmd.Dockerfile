FROM debian:12-slim

# Debian setup
RUN sed -i "s/main/main contrib non-free/g" /etc/apt/sources.list.d/debian.sources
ARG DEBIAN_FRONTEND=noninteractive
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# SteamCMD setup
RUN echo steam steam/question select "I AGREE" | debconf-set-selections \
 && echo steam steam/license note '' | debconf-set-selections

RUN dpkg --add-architecture i386 \
 && apt-get update -y \
 && apt-get install -y --no-install-recommends ca-certificates locales steamcmd libsdl2-2.0-0:i386 sudo
RUN ln -s /usr/games/steamcmd /usr/bin/steamcmd

# Locale setup
RUN locale-gen en_US.UTF-8
ENV LANG 'en_US.UTF-8'
ENV LANGUAGE 'en_US:en'

# User setup
RUN adduser --disabled-password steam
RUN usermod -aG sudo steam
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# Image setup
ENV INSTALL_DIR="/home/steam/server"
ENV INSTALL=${INSTALL:-"1"}

RUN mkdir -p /home/steam/server && chown -R steam:steam /home/steam/server

USER steam
WORKDIR /home/steam

COPY --chown=steam:steam entrypoint.sh /home/steam/entrypoint.sh
RUN chmod +x /home/steam/entrypoint.sh

# Pre update SteamCMD to latest version
RUN steamcmd +quit

ENTRYPOINT ["/home/steam/entrypoint.sh"]
