#!/bin/bash

if [ "$INSTALL" == "4" ]; then
    echo "Skipping update"
    exit 0
fi

if [ "$INSTALL" == "0" ]; then
    echo "Skipping install"
else
  test -n "$APP_ID" || { echo "APP_ID not set"; exit 1; }
  steamcmd +force_install_dir "$INSTALL_DIR" +login anonymous +app_update "$APP_ID" validate +quit
fi

if [ "$INSTALL" == "2" ]; then
    exit 0
fi

exec "$@"