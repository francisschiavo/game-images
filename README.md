# Docker images

![pipeline status](https://gitlab.com/francisschiavo/docker-images/badges/master/pipeline.svg)

This repository contains the Dockerfiles for the images used in my game servers.

## Images

### SteamCMD

Base SteamCMD image for running containerized dedicated servers.

Available environment variables:

| VAR             | DEFAULT VALUE | USAGE                                                            |
|-----------------|---------------|------------------------------------------------------------------|
| APP_ID          | NONE          | APP_ID for the dedicated server                                  |
| USER            | steam         | User to run the server as, useful for changing volume data owner |
| UID             | 1000          | Linux UID                                                        |

### Project Zomboid

Allows to run a Project Zomboid server.

Available environment variables:

| VAR            | DEFAULT VALUE | USAGE          |
|----------------|---------------|----------------|
| SERVER_NAME    | PZ server     | Server name    |
| ADMIN_PASSWORD | PZAdmin       | Admin password |

## Local testing

For convenience, we can use the script `local-images.sh` to build the images locally.
This is useful for testing changes to the images prior to pushing them
 and having to wait for the CI pipeline to run.

This also prevents us from pushing unstable images to the container registry.
