#!/bin/bash

if [ ! -L /home/steam/server/Engine/Binaries/ThirdParty/SteamCMD/Linux ]; then
  echo "Fixing SteamCMD paths"
  ln -s /home/steam/.local/share/Steam/steamcmd /home/steam/server/Engine/Binaries/ThirdParty/SteamCMD/Linux
  ln -s /home/steam/.local/share/Steam/steamapps /home/steam/server/Engine/Binaries/ThirdParty/SteamCMD/Linux/steamapps
fi

cd server/ShooterGame/Binaries/Linux || exit
echo "Starting server $SESSION_NAME ($SAVE_DIR)"
ulimit -n 100000
./ShooterGameServer "$MAP?listen?SessionName=$SESSION_NAME?MaxPlayers=$MAX_PLAYERS?ServerPassword=$SERVER_PASSWORD?ServerAdminPassword=$ADMIN_PASSWORD?PreventDownloadSurvivors=False?PreventDownloadItems=False?PreventDownloadDinos=False?PreventUploadSurvivors=False?PreventUploadItems=False?PreventUploadDinos=False?noTributeDownloads=False?Port=$PORT?QueryPort=$QUERY_PORT?RCONPort=$RCON_PORT?Multihome=$MULTIHOME?AltSaveDirectoryName=$SAVE_DIR" -clusterid="$CLUSTER_ID" -server -log -automanagedmods
