FROM registry.gitlab.com/francisschiavo/game-images/steamcmd:latest

ENV APP_ID=376030
ENV SESSION_NAME="Ark Server"
ENV MAP=TheIsland
ENV MAX_PLAYERS=10
ENV SERVER_PASSWORD=@rkServer
ENV ADMIN_PASSWORD=@rkAdmin
ENV CLUSTER_ID=Arkluster
ENV SAVE_DIR=the_island
ENV MULTIHOME=0.0.0.0
ENV PORT=${PORT:-7777}
ENV PORT2=${PORT2:-7778}
ENV QUERY_PORT=${QUERY_PORT:-27015}
ENV RCON_PORT=${RCON_PORT:-27020}

COPY --chown=steam:steam start.sh /home/steam/start.sh
RUN chmod +x /home/steam/start.sh

EXPOSE $PORT/udp $PORT2/udp $QUERY_PORT/udp $RCON_PORT/tcp

CMD ["/home/steam/start.sh"]
