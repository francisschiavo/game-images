FROM registry.gitlab.com/francisschiavo/game-images/steamcmd:latest

ENV APP_ID=380870
ENV SERVER_NAME="PZ server"
ENV ADMIN_PASSWORD="PZAdmin"
ENV PORT=16261
ENV PORT2=16262

COPY --chown=steam:steam start.sh /home/steam/start.sh
RUN chmod +x /home/steam/start.sh

EXPOSE $PORT/udp $PORT2/udp

CMD ["/home/steam/start.sh"]
