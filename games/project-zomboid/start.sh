#!/bin/bash

echo "Starting server $SERVER_NAME"
./server/start-server.sh -adminpassword "$ADMIN_PASSWORD" -servername "$SERVER_NAME" -port "$PORT" -udpport "$PORT2"
