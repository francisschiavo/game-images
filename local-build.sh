# Repository
REPOSITORY="registry.gitlab.com/francisschiavo/game-images"

# Steam CMD
cd steamcmd || exit 1
docker build --no-cache -t "$REPOSITORY/steamcmd:latest" -f steamcmd.Dockerfile .
if [ "$?" -ne 0 ]; then
    echo "Failed to build SteamCMD image"
    exit 1
fi
cd ..

# Games
cd games || exit 1

## Project Zomboid
cd project-zomboid || exit 1
docker build --no-cache -t "$REPOSITORY/games/project-zomboid:latest" -f project-zomboid.Dockerfile .
cd ..

## ARK: Survival Evolved
cd ark-survival-evolved || exit 1
docker build --no-cache -t "$REPOSITORY/games/ark-survival-evolved:latest" -f ark-survival-evolved.Dockerfile .